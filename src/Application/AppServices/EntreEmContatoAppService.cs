﻿using Domain.Interfaces.AppServices;
using Domain.Interfaces.Emails;
using Domain.Interfaces.Services;
using Domain.Models;

namespace Application.AppServices
{
    public class EntreEmContatoAppService : BaseAppService<EntreEmContato>, IEntreEmContatoAppService
    {
        private readonly IEntreEmContatoService _entreEmContatoService;
        private readonly IEntreEmContatoEmailService _entreEmContatoEmailService;

        public EntreEmContatoAppService(IEntreEmContatoService entreEmContatoService, IEntreEmContatoEmailService entreEmContatoEmailService)
            : base(entreEmContatoService)
        {
            _entreEmContatoService = entreEmContatoService;
            _entreEmContatoEmailService = entreEmContatoEmailService;
        }

        public void Enviar(EntreEmContato entreEmContato)
        {
            _entreEmContatoEmailService.Enviar(entreEmContato);
            //_entreEmContatoService.Add(entreEmContato);
        }
    }
}
