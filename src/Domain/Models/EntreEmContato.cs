﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public class EntreEmContato : Base
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Mensagem { get; set; }

        public string HorarioParaContato { get; set; }
    }
}