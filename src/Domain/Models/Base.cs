﻿using System;

namespace Domain.Models
{
    public class Base
    {
        public int Id { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }
    }
}
