﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class LoginNotFoundException : Exception
    {
        public LoginNotFoundException(string message) : base(message) { }

        public LoginNotFoundException(string message, Exception ex) : base(message, ex) { }

        public LoginNotFoundException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public LoginNotFoundException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
