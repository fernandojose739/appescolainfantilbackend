﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message) { }

        public NotFoundException(string message, Exception ex) : base(message, ex) { }

        public NotFoundException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public NotFoundException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
