﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class DuplicateException : Exception
    {
        public DuplicateException(string message) : base(message) { }

        public DuplicateException(string message, Exception ex) : base(message, ex) { }

        public DuplicateException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public DuplicateException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
