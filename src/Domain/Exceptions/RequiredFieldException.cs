﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class RequiredFieldException : Exception
    {
        public RequiredFieldException(string message) : base(message) { }

        public RequiredFieldException(string message, Exception ex) : base(message, ex) { }

        public RequiredFieldException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public RequiredFieldException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
