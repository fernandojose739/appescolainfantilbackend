﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class NoContentException : Exception
    {
        public NoContentException(string message) : base(message) { }

        public NoContentException(string message, Exception ex) : base(message, ex) { }

        public NoContentException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public NoContentException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
