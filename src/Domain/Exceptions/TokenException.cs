﻿using System;
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Exceptions
{
    public sealed class TokenException : Exception
    {
        public TokenException(string message) : base(message) { }

        public TokenException(string message, Exception ex) : base(message, ex) { }

        public TokenException(ICollection<Error> errors)
        {
            Errors = errors;
        }

        public TokenException(Error erro)
        {
            Erro = erro;
        }

        public Error Erro { get; private set; }

        public ICollection<Error> Errors { get; private set; }
    }
}
