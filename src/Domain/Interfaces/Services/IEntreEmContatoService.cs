﻿using Domain.Models;

namespace Domain.Interfaces.Services
{
    public interface IEntreEmContatoService : IBaseService<EntreEmContato>
    {
    }
}
