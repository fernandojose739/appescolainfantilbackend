﻿using Domain.Models;

namespace Domain.Interfaces.AppServices
{
    public interface IEntreEmContatoAppService : IBaseAppService<EntreEmContato>
    {
        void Enviar(EntreEmContato entreEmContato);
    }
}
