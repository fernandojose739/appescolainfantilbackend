﻿using Domain.Models;

namespace Domain.Interfaces.Emails
{
    public interface IEntreEmContatoEmailService
    {
        bool Enviar(EntreEmContato entreEmContato);
    }
}