﻿namespace Domain.Entities
{
    public class Error
    {
        public string Key { get; set; }

        public string ErrorMessage { get; set; }
    }
}
