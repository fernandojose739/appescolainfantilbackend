﻿using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Domain.Models;

namespace Domain.Services
{
    public class EntreEmContatoService : BaseService<EntreEmContato>, IEntreEmContatoService
    {
        private readonly IEntreEmContatoRepository _entreEmContatoRepository;

        public EntreEmContatoService(IEntreEmContatoRepository entreEmContatoRepository)
            : base(entreEmContatoRepository)
        {
            _entreEmContatoRepository = entreEmContatoRepository;
        }
    }
}
