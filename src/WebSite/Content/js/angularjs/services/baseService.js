﻿(function () {
    'use strict';

    meuApp.factory('baseService', BaseService);

    BaseService.$inject = ['configApp', '$http'];

    function BaseService(configApp, $http) {

        var dataFactory = {};

        dataFactory.get = function (url) {

            var req = {
                method: 'GET',
                url: configApp.serverUrl + url,
                headers: {
                    'Content-Type': 'application/json',
                    'Token': '8518c3c1-adca-4a18-85f2-5f6ac32d097a'
                },
            }

            return $http(req);
        };

        dataFactory.post = function (url, request) {

            var req = {
                method: 'POST',
                url: configApp.serverUrl + url,
                headers: {
                    'Content-Type': 'application/json',
                    'Token': '8518c3c1-adca-4a18-85f2-5f6ac32d097a'
                },
                data: JSON.stringify(request)
            }

            return $http(req);
        };

        return dataFactory;
    }
})();