﻿using System.Web;
using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/content/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/content/js/bootstrap.js",
                      "~/content/js/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularJs").Include("~/Content/js/angularJs/lib/angular.js",
                                                                        "~/Content/js/angularJs/lib/angular-sanatize.js",
                                                                        "~/Content/js/angularJs/lib/angular-animate.js",
                                                                        "~/Content/js/angularJs/module/app.js",
                                                                        "~/Content/js/angularJs/controllers/entreEmContatoController.js",
                                                                        "~/Content/js/angularJs/services/baseService.js",
                                                                        "~/Content/js/angularJs/services/entreEmContatoService.js"));
            bundles.Add(new ScriptBundle("~/bundles/angularJsModule").Include("~/Content/js/angularJs/module/app.js"));
            bundles.Add(new ScriptBundle("~/bundles/angularJsController").Include("~/Content/js/angularJs/controllers/entreEmContatoController.js"));
            bundles.Add(new ScriptBundle("~/bundles/angularJsService").Include("~/Content/js/angularJs/services/baseService.js",
                                                                               "~/Content/js/angularJs/services/entreEmContatoService.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/content/css/bootstrap.css",
                      "~/content/css/navbar.css",
                      "~/content/css/components.css",
                      "~/content/css/header.css",
                      "~/content/css/home.css",
                      "~/content/css/footer.css"));
        }
    }
}
