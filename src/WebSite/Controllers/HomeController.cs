﻿using System.Web.Mvc;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(Duration = 1440, VaryByParam = "none")]
        public ActionResult Index()
        {
            return View();
        }
    }
}