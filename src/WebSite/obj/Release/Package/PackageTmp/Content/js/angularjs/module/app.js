﻿'use strict';
var meuApp = angular.module('meuApp', ['ngSanitize', 'ngAnimate']);

meuApp.constant("configApp", {
    "serverUrl": "http://localhost:8766/",
    "port": "80"
});