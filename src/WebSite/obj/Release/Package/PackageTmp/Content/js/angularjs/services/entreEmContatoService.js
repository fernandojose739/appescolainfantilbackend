﻿(function () {
    'use strict';

    meuApp.service('entreEmContatoService', entreEmContatoService);

    entreEmContatoService.$inject = ['baseService',
							         '$q'];

    function entreEmContatoService(baseService, $q) {

        var service = this;

        service.enviar = enviar;

        return service;

        function enviar(request) {
            var def = $q.defer();
            baseService.post("entre-em-contato/enviar", request)
				.then(function (data) {
				    def.resolve(data);
				}, function (error) {
				    def.reject("Error: " + error);
				});
            return def.promise;
        };
    }
})();