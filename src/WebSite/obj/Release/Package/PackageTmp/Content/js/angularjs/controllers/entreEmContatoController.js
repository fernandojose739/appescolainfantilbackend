﻿(function () {
    'use strict';

    meuApp.controller('entreEmContatoController', entreEmContatoController);

    entreEmContatoController.$inject = [
        'entreEmContatoService'
    ];

    function entreEmContatoController(entreEmContatoService) {

        var ctrl = this;

        setEvents();
        setProperties();
        initialize();

        return ctrl;

        function setEvents() {
            ctrl.enviar = enviar;
        };

        function setProperties() {
            resetForm();
            ctrl.carregando = false;
            ctrl.alerta = '';
        };

        function initialize() {
        };

        function resetForm() {
            ctrl.form = {
                Nome: '',
                Email: '',
                Telefone: '',
                HorarioParaContato: '',
                Mensagem: ''
            };
        };

        function enviar() {
            ctrl.carregando = true;
            entreEmContatoService.enviar(ctrl.form).then(function (data) {
                ctrl.alerta = 'Mensagem enviada com sucesso. Entraremos em contato para mais informações.';
                resetForm();
                ctrl.carregando = false;
            }, function (data) {
                ctrl.alerta = 'Falha ao enviar a mensagem. Favor tentar novamente mais tarde.';
                ctrl.carregando = false;
            });
        };
    }
})();