﻿using Api.Filters.Security;
using Domain.Interfaces.AppServices;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using Domain.Models;

namespace Api.Controllers
{
    [RoutePrefix("entre-em-contato")]
    public class EntreEmContatoController : ApiController
    {
        private readonly IEntreEmContatoAppService _entreEmContatoAppService;

        public EntreEmContatoController(IEntreEmContatoAppService entreEmContatoAppService)
        {
            _entreEmContatoAppService = entreEmContatoAppService;
        }

        [HttpPost]
        [BasicAuthentication]
        [Route("enviar")]
        public HttpResponseMessage Listar(EntreEmContato entreEmContato)
        {
            _entreEmContatoAppService.Enviar(entreEmContato);
            return Request.CreateResponse(HttpStatusCode.OK, "mensagem enviada com sucesso");
        }
    }
}
