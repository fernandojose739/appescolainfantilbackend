﻿using Domain.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace Api.Filters.Exception
{
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            base.OnException(context);
            var httpStatusCode = GetHttpStatusCode(context.Exception.GetType());
            context.Response = context.Request.CreateResponse(httpStatusCode, new
            {
                context.Exception.Message,
                Description = context.Exception.ToString(),
                Date = DateTime.Now
            });

            context.Response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                NoCache = true,
                NoStore = true
            };
        }

        public HttpStatusCode GetHttpStatusCode(Type type)
        {
            if (type == typeof(NoContentException))
            {
                return HttpStatusCode.NoContent;
            }

            if (type == typeof(TokenException))
            {
                return HttpStatusCode.Unauthorized;
            }

            if (type == typeof(NotFoundException))
            {
                return HttpStatusCode.NotFound;
            }

            return HttpStatusCode.InternalServerError;
        }
    }
}