﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace Api.Filters.Security
{
    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var headers = actionContext.Request.Headers;
            var hasToken = headers.Contains("Token");
            if (!hasToken)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Request não contém o token!");
            }

            var tokenHeader = headers.GetValues("Token").FirstOrDefault();
            var tokenWebConfig = System.Configuration.ConfigurationManager.AppSettings["token"];

            if (tokenHeader != tokenWebConfig)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Request com o token inválido!");
            }
        }
    }
}