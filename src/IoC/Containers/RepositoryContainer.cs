﻿using Data.Repositories;
using Domain.Interfaces.Repositories;
using Ninject;

namespace IoC.Containers
{
    public class RepositoryContainer
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<IEntreEmContatoRepository>().To<EntreEmContatoRepository>();
        }
    }
}
