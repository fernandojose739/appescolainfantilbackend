﻿using Domain.Interfaces.Emails;
using Email.Services;
using Ninject;

namespace IoC.Containers
{
    public class EmailContainer
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<IEntreEmContatoEmailService>().To<EntreEmContatoEmailService>();
        }
    }
}
