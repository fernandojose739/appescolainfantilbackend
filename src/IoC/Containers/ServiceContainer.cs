﻿using Domain.Interfaces.Services;
using Domain.Services;
using Ninject;

namespace IoC.Containers
{
    public class ServiceContainer
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<IEntreEmContatoService>().To<EntreEmContatoService>();
        }
    }
}
