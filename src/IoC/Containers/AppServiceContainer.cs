﻿using Application.AppServices;
using Domain.Interfaces.AppServices;
using Ninject;

namespace IoC.Containers
{
    public class AppServiceContainer
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<IEntreEmContatoAppService>().To<EntreEmContatoAppService>();
        }
    }
}
