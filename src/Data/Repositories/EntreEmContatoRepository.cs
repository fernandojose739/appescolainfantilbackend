﻿using Domain.Interfaces.Repositories;
using Domain.Models;

namespace Data.Repositories
{
    public class EntreEmContatoRepository : BaseRepository<EntreEmContato>, IEntreEmContatoRepository
    {
    }
}
