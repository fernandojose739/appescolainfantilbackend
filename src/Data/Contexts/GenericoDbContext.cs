﻿using Data.EntityMapping;
using Domain.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Data.Contexts
{
    public class GenericoDbContext : DbContext
    {
        public GenericoDbContext()
            : base(nameOrConnectionString: "ApiConnection")
        { }

        public DbSet<EntreEmContato> EntreEmContato { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType(("varchar")));
            modelBuilder.Properties<string>().Configure(x => x.HasMaxLength(100));

            modelBuilder.Configurations.Add(new EntreEmContatoMap());            
        }
    }
}
