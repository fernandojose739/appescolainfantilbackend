﻿using System.Data.Entity.ModelConfiguration;
using Domain.Models;

namespace Data.EntityMapping
{
    public class EntreEmContatoMap : EntityTypeConfiguration<EntreEmContato>
    {
        public EntreEmContatoMap()
        {
            //--- Base
            HasKey(t => t.Id);
            Property(t => t.Id);
            Property(t => t.CreatedDate).HasColumnType("DateTime");
            Property(t => t.UpdatedDate).HasColumnType("DateTime");
            Property(t => t.CreatedBy).HasMaxLength(200);

            //--- Especifico
            Property(t => t.Nome).HasMaxLength(200);
            Property(t => t.Email).HasMaxLength(200);
            Property(t => t.Telefone).HasMaxLength(50);
            Property(t => t.HorarioParaContato).HasMaxLength(200);
            Property(t => t.Mensagem).IsMaxLength();
        }
    }
}
