adminApp.config([
    '$routeProvider',
    function($routeProvider) {

        $routeProvider
            .when('/login',
            {
                templateUrl: '../views/login/login.html',
                controller: 'loginController',
                controllerAs: 'loginCtrl'
            })
            .when('/atividades',
            {
                templateUrl: '../views/atividade/atividades.html',
                controller: 'atividadesController',
                controllerAs: 'atividadesCtrl',
                resolve: {
                    auth: [
                        "authenticationService", function (authenticationService) {
                            return authenticationService.getUserInfo();
                        }
                    ]
                }
            })
            .when('/anuncio/:anuncioId',
            {
                templateUrl: '../views/anuncio/form.html',
                controller: 'anuncioController',
                controllerAs: 'anuncioCtrl',
                resolve: {
                    auth: [
                        "authenticationService", function (authenticationService) {
                            return authenticationService.getUserInfo();
                        }
                    ]
                }
            })
            .when('/usuario',
            {
                templateUrl: '../views/usuario/form.html',
                controller: 'usuarioController',
                controllerAs: 'usuarioCtrl',
                resolve: {
                    auth: [
                        "authenticationService", function (authenticationService) {
                            return authenticationService.getUserInfo();
                        }
                    ]
                }
            })
            .otherwise({
                redirectTo: '/landing'
            });
    }
]);
