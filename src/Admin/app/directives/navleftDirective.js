(function () {
    'use strict';

    adminApp.directive('navLeft', navleftDirective);

    navleftDirective.$inject = [];

    function navleftDirective() {

        var directive = {};

        directive.templateUrl = "../views/shared/nav-left.html";

        return directive; 
    }
})();

