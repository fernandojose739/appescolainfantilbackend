(function () {
    'use strict';

    adminApp.directive('header', headerDirective);

    headerDirective.$inject = [];

    function headerDirective() {
        var directive = {};
        directive.templateUrl = "../views/shared/header.html"; 
        return directive; 
    }
})();

