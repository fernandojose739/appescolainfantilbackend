var adminApp = angular.module('adminApp', ['ngRoute', 'textAngular']);

adminApp.run(["$rootScope", "$location", 'localStorageService', function ($rootScope, $location, localStorageService) {
    $rootScope.$on("$routeChangeSuccess", function (userInfo) {
        $rootScope.loginPage = (window.location.hash === "#/login")
        $rootScope.pageLocation = window.location.hash.split('/')[1];

        $rootScope.logout = function () {
            localStorageService.remove('userInfo');
            $location.path("/login");
        }
    });
}]);