(function () {
    'use strict';

    adminApp.service('usuarioService', usuarioService);

    usuarioService.$inject = ['baseService', '$q', 'localStorageService'];

    function usuarioService(baseService, $q, localStorageService) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.list = list;
            service.get = get;
            service.getInLocalStorage = getInLocalStorage;
            service.save = save;
        }

        function list() {

            var def = $q.defer();

            var url = "/usuario/listar";

            baseService.call("GET", url, "")
                .then(function (data) {
                    def.resolve(data);
                },
                function (error) {
                    def.reject("Error: " + error);
                });

            return def.promise;
        }

        function getInLocalStorage() {
            var usuario = localStorageService.get('usuario');
            if (usuario) {
                return JSON.parse(usuario);
            }

            return undefined;
        };

        function get(usuarioId) {

            var def = $q.defer();

            var url = "/usuario/obter/" + usuarioId;

            baseService.call("GET", url, "")
                .then(function (data) {
                    def.resolve(data);
                },
                function (error) {
                    def.reject("Error: " + error);
                });

            return def.promise;
        }

        function save(request, files) {

            var def = $q.defer();
            var url = "/usuario/salvar/integrador";

            baseService.saveWithUpload("POST", url, request, files)
            .then(function (data) {
                def.resolve(data);
            }, function (error) {
                def.reject("Error: " + error);
            });

            return def.promise;
        }
    }
})();