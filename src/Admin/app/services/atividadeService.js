(function() {
    'use strict';

    adminApp.service('atividadeService', atividadeService);

    atividadeService.$inject = ['baseService', '$q', 'usuarioService'];

    function atividadeService(baseService, $q, usuarioService) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.get = get;
            service.listar = listar;
            service.save = save;
            service.upload = upload;
        }

        function get(anuncioId) {

            var def = $q.defer();

            var url = "/anuncio/get/" + anuncioId;

            baseService.call("GET", url, "")
                .then(function(data) {
                        def.resolve(data);
                    },
                    function(error) {
                        def.reject("Error: " + error);
                    });

            return def.promise;
        }

        function listar() {
            var def = $q.defer();
            var usuario = usuarioService.getInLocalStorage();
            var url = "/atividade/listar/" + usuario.Escola.Id;

            baseService.call("GET", url, "")
            .then(function (data) {
                def.resolve(data);
            }, function (error) {
                def.reject("Error: " + error);
            });

            return def.promise;
        }

        function save(value) {

            var def = $q.defer();

            var url = "/integrador/anuncio/salvar";

            baseService.call("POST", url, value)
            .then(function (data) {
                def.resolve(data);
            }, function (error) {
                def.reject("Error: " + error);
            });

            return def.promise;
        }

        function upload(model, files) {

            var def = $q.defer();

            var url = "/integrador/anuncio/upload-image";

            baseService.saveWithUpload("POST", url, model, files)
            .then(function (data) {
                def.resolve(data);
            }, function (error) {
                def.reject("Error: " + error);
            });

            return def.promise;
        }
    }
})();