(function() {
    'use strict';

    adminApp.service('modeloService', modeloService);

    modeloService.$inject = ['baseService', '$q'];

    function modeloService(baseService, $q) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.listarModeloPorMarca = listarModeloPorMarca;
        }

        function listarModeloPorMarca(marcaId) {

            var def = $q.defer();

            var url = "/listar-modelo-por-marca/" + marcaId;

            baseService.call("GET", url, "")
                .then(function(data) {
                        def.resolve(data);
                    },
                    function(error) {
                        def.reject("Error: " + error);
                    });

            return def.promise;
        }
    }
})();