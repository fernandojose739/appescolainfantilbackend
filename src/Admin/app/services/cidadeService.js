(function() {
    'use strict';

    adminApp.service('cidadeService', cidadeService);

    cidadeService.$inject = ['baseService', '$q'];

    function cidadeService(baseService, $q) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.listarCidadePorEstado = listarCidadePorEstado;
        }

        function listarCidadePorEstado(estadoId) {

            var def = $q.defer();

            var url = "/listar-cidade-por-estado/" + estadoId;

            baseService.call("GET", url, "")
                .then(function(data) {
                        def.resolve(data);
                    },
                    function(error) {
                        def.reject("Error: " + error);
                    });

            return def.promise;
        }
    }
})();