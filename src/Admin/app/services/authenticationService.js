(function () {
    'use strict';

    adminApp.service('authenticationService', authentication);

    authentication.$inject = ['localStorageService', "$location"];

    function authentication(localStorageService, $location) {

        var service = this;
        var userInfo;

        service.getUserInfo = getUserInfo;

        return service;

        function getUserInfo() {
            var userInfo = localStorageService.get("usuario");
            if (!userInfo) {
                $location.path("/login");
            }               
        }
    }
})();
