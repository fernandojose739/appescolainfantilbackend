(function () {
    'use strict';

    adminApp.factory('baseService', BaseService);

    BaseService.$inject = ['$http',
                           'webConfig'];

    function BaseService($http, webConfig) {

        var dataFactory = {};

        dataFactory.call = function (method, url, request) {

            var req = {
                method: method,
                url: webConfig.urlApi + url,
                headers: {
                    'Content-Type': 'application/json',
                    'token': webConfig.token,
                },
                data: JSON.stringify(request)
            }

            return $http(req);
        };

        dataFactory.saveWithUpload = function (method, url, request, files) {

            var req = {
                method: 'POST',
                url: webConfig.urlApi + url,
                headers: {
                    'Content-Type': undefined,
                    'token': webConfig.token,
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));
                    for (var i = 0; i < data.files.length; i++) {

                        if (data.files[i].propertyName != null && data.files[i].propertyName != undefined) {
                            formData.append(data.files[i].propertyName, data.files[i]);
                        } else {
                            formData.append("file" + i, data.files[i]);
                        }
                    }
                    return formData;
                },
                data: { model: request, files: files }
            }

            return $http(req);
        };

        return dataFactory;
    }
})();
