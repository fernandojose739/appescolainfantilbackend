(function() {
    'use strict';

    adminApp.service('loginService', LoginService);

    LoginService.$inject = ['baseService', '$q', 'localStorageService'];

    function LoginService(baseService, $q, localStorageService) {

        var service = this;

        service.Login = login;

        return service;

        function login(value) {
            
            var def = $q.defer();
            var url = "/usuario/login/" + value.email + "/" + value.password;

            baseService.call("GET", url, "")
            .then(function(data) {
                def.resolve(data); 
                localStorageService.add("usuario", data.data);
            }, function(error) {
                def.reject("Error: " + error);
            });
            
            return def.promise;
        }

    }
})();