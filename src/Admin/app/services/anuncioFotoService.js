(function() {
    'use strict';

    adminApp.service('anuncioFotoService', anuncioFotoService);

    anuncioFotoService.$inject = ['baseService', '$q'];

    function anuncioFotoService(baseService, $q) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.remover = remover;
        }

        function remover(value) {

            var def = $q.defer();

            var url = "/integrador/anuncio-foto/remover";

            baseService.call("DELETE", url, value)
            .then(function (data) {
                def.resolve(data);
            }, function (error) {
                def.reject("Error: " + error);
            });

            return def.promise;
        }
    }
})();