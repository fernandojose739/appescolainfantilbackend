(function() {
    'use strict';

    adminApp.service('versaoService', versaoService);

    versaoService.$inject = ['baseService', '$q'];

    function versaoService(baseService, $q) {

        var service = this;

        setEvents();

        return service;

        function setEvents() {
            service.listarVersaoPorModeloEAno = listarVersaoPorModeloEAno;
        }

        function listarVersaoPorModeloEAno(modeloId, anoFabricacao, anoModelo) {

            var def = $q.defer();

            var url = "/listar-versao-por-modelo-e-ano/" + modeloId + "/" + anoFabricacao + "/" + anoModelo;

            baseService.call("GET", url, "")
                .then(function(data) {
                        def.resolve(data);
                    },
                    function(error) {
                        def.reject("Error: " + error);
                    });

            return def.promise;
        }
    }
})();