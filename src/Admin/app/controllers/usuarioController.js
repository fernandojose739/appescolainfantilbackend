(function () {
    'use strict';

    adminApp.controller('usuarioController', usuarioController);

    usuarioController.$inject = ['usuarioService', '$location', '$scope', 'localStorageService', 'webConfig'];

    function usuarioController(usuarioService, $location, $scope, localStorageService, webConfig) {
        var ctrl = this;

        setProperties();
        setEvents();
        init();

        return ctrl;

        function setProperties() {
            ctrl.urlApi = webConfig.urlApi;
            ctrl.arquivos = [];
            ctrl.usuario = {
                Id: '',
                NomeCompleto: '',
                TipoPessoa: '',
                Cpf: '',
                Cnpj: '',
                Email: '',
                Cep: '',
                Numero: '',
                Complemento: '',
                Telefone: '',
                Celular: '',
                DataDeNascimento: '',
                Logradouro: '',
                Bairro: '',
                Estado: '',
                Cidade: '',
                TipoId: '',
                Senha: '',
            };
        }

        function setEvents() {
            ctrl.save = save;
        }

        function init() {
            get();
            setEventSelectedFile();
        }

        function get() {
            ctrl.usuario = usuarioService.getInLocalStorage();
        }

        function save() {
            usuarioService.save(ctrl.usuario, ctrl.arquivos).then(function (data) {
                ctrl.usuario = data.data;
                ctrl.arquivos = [];
                localStorageService.add("usuario", data.data);
                alert('Usu�rio atualizado com sucesso!');
            }, function (data) {
                alert(data);
            });
        }

        function setEventSelectedFile() {
            $scope.$on("selectedFile", function (event, args) {
                $scope.$apply(function () {
                    ctrl.arquivos.push(args.file);
                });
            });
        }
    }
})();
