(function() {
    'use strict';

    adminApp.controller('loginController', loginController);

    loginController.$inject = ['loginService', '$location'];

    function loginController(loginService, $location) {
        var ctrl = this;

        setProperties();
        setEvents();

        return ctrl;

        function setProperties() {
            ctrl.form = {
                email: '',
                password: ''
            };

            ctrl.carregando = false;
            ctrl.alerta = "";
        }

        function setEvents() {
            ctrl.login = login;
        }

        function login() {
            ctrl.carregando = true;
            ctrl.alerta = "";

            loginService.Login(ctrl.form).then(function(data) {
                $location.path('/landing');
                ctrl.carregando = false;
            }, function () {
                ctrl.carregando = false;
                ctrl.alerta = "Login ou senha inválido";
            });
        }
    }
})();
