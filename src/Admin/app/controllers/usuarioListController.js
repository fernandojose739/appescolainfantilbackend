(function () {
    'use strict';

    adminApp.controller('usuarioListController', usuarioListController);

    usuarioListController.$inject = ['usuarioService'];

    function usuarioListController(usuarioService) {

        var ctrl = this;

        setProperties();
        setEvents();
        init();

        return ctrl;

        function setProperties() {
            ctrl.usuarios = []; 
        }

        function setEvents() {

        }

        function init() {
            list();
        }

        function list() {
            usuarioService.list().then(function (data) {
                ctrl.usuarios = data.data;
            }, function () {
                console.log("error");
            });
        }
    }
})();
