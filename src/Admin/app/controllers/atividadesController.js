(function () {
    'use strict';

    adminApp.controller('atividadesController', atividadesController);

    atividadesController.$inject = ['atividadeService'];

    function atividadesController(atividadeService) {

        var ctrl = this;

        setProperties();
        setEvents();
        init();

        return ctrl;

        function setProperties() {
            ctrl.atividades = []; 
        }

        function setEvents() {

        }

        function init() {
            listar();
        }

        function listar() {
            atividadeService.listar().then(function (data) {
                ctrl.atividades = data.data;
            }, function () {
                console.log("error");
            });
        }
    }
})();
