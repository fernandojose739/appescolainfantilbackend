(function () {
    'use strict';

    adminApp.controller('anuncioController', anuncioController);

    anuncioController.$inject = ['anuncioService', '$routeParams', 'modeloService', 'versaoService', 'cidadeService', '$location', '$scope', 'anuncioFotoService'];

    function anuncioController(anuncioService, $routeParams, modeloService, versaoService, cidadeService, $location, $scope, anuncioFotoService) {

        var ctrl = this;

        setProperties();
        setEvents();
        init();

        return ctrl;

        function setProperties() {
            ctrl.anuncioId = $routeParams.anuncioId;
            ctrl.anuncio = {};
            ctrl.fotos = [];
        }

        function setEvents() {
            ctrl.listarModeloPorMarca = listarModeloPorMarca;
            ctrl.listarVersaoPorModeloEAno = listarVersaoPorModeloEAno;
            ctrl.listarCidadePorEstado = listarCidadePorEstado;
            ctrl.salvar = salvar;
            ctrl.removerFoto = removerFoto;
            ctrl.upload = upload;
        }

        function init() {
            get();
            setEventSelectedFile();
        }

        function get() {
            anuncioService.get(ctrl.anuncioId).then(function (data) {
                ctrl.anuncio = data.data;
            }, function () {
                console.log("error");
            });
        }

        function listarModeloPorMarca() {
            ctrl.anuncio.ModelosViewModel = [];
            ctrl.anuncio.VersoesViewModel = [];

            if (ctrl.anuncio.ModeloViewModel)
                ctrl.anuncio.ModeloViewModel.Id = 0;

            if (ctrl.anuncio.VersaoViewModel)
                ctrl.anuncio.VersaoViewModel.Id = 0;

            if (ctrl.anuncio.MarcaViewModel.Id == 0) return;

            modeloService.listarModeloPorMarca(ctrl.anuncio.MarcaViewModel.Id).then(function (data) {
                ctrl.anuncio.ModelosViewModel = data.data;
            });
        }

        function listarVersaoPorModeloEAno() {
            ctrl.anuncio.VersoesViewModel = [];

            if (ctrl.anuncio.VersaoViewModel)
                ctrl.anuncio.VersaoViewModel.Id = 0;

            if (ctrl.anuncio.ModeloViewModel.Id === 0 ||
                ctrl.anuncio.AnoFabricacao === '' ||
                ctrl.anuncio.AnoModelo === '') return;

            versaoService.listarVersaoPorModeloEAno(ctrl.anuncio.ModeloViewModel.Id, ctrl.anuncio.AnoFabricacao, ctrl.anuncio.AnoModelo).then(function (data) {
                ctrl.anuncio.VersoesViewModel = data.data;
            });
        }

        function listarCidadePorEstado() {
            ctrl.anuncio.CidadesViewModel = [];
            ctrl.anuncio.CidadeId = 0;

            if (ctrl.anuncio.EstadoId === 0) return;

            cidadeService.listarCidadePorEstado(ctrl.anuncio.EstadoId).then(function (data) {
                ctrl.anuncio.CidadesViewModel = data.data;
            });
        }

        function salvar() {
            anuncioService.save(ctrl.anuncio).then(
            function (data) {
                $location.path('/anuncios');
            }, function (data) {
                alert(data);
            });
        }

        function setEventSelectedFile() {
            $scope.$on("selectedFile", function (event, args) {
                $scope.$apply(function () {
                    ctrl.fotos.push(args.file);
                });
            });
        };

        function upload() {
            if (ctrl.fotos.length <= 0) return;

            anuncioService.upload(ctrl.anuncio, ctrl.fotos)
                .then(function (data) {
                    ctrl.anuncio.AnuncioFotosViewModel = data.data;
                    ctrl.fotos = [];
                }, function (error) {
                    alert(error);
                });
        }

        function removerFoto(anuncioFotoId, arquivo) {

            var anuncioFoto = {
                Id: anuncioFotoId,
                AnuncioId: ctrl.anuncio.Id,
                Arquivo: arquivo
            };

            anuncioFotoService.remover(anuncioFoto).then(
            function (data) {
                ctrl.anuncio.AnuncioFotosViewModel = data.data;
            }, function (data) {
                alert(data);
            });
        }
    }
})();
