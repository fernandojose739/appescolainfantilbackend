﻿using Domain.Entities;
using Domain.Interfaces.Emails;
using Domain.Models;

namespace Email.Services
{
    public class EntreEmContatoEmailService : IEntreEmContatoEmailService
    {
        public bool Enviar(EntreEmContato entreEmContato)
        {
            var body = "<!DOCTYPE html>" +
                          "<html>" +
                          "<head>" +
                          "    <meta name='viewport' content='width=device-width' />" +
                          "    <title>Fale Conosco</title>" +
                          "</head>" +
                          "<body>" +
                          "    <div style='width: 750px; border: 1px solid #e2e2e2; padding: 15px 15px; background-color: #fafafa; font-size: verdana;'>" +
                          "        <div style='width: 745px; height: 74px; background-color: #666666; text-align: center; -moz-border-radius: 7px; -webkit-border-radius: 7px; border-radius: 7px; padding-top: 20px; margin-left: 1px;'>" +
                          "            <p style='color: #000;'>nome: " + entreEmContato.Nome + "</p>" +
                          "            <p style='color: #000;'>e-mail: " + entreEmContato.Email + "</p>" +
                          "            <p style='color: #000;'>telefone: " + entreEmContato.Telefone + "</p>" +
                          "            <p style='color: #000;'>melhor horário para contato: " + entreEmContato.HorarioParaContato + "</p>" +
                          "            <p style='color: #000;'>mensagem:  " + entreEmContato.Mensagem + "</p>" +
                          "        </div>" +
                          "    </div>" +
                          "</body>" +
                          "</html>";

            var emailEntity = new EmailEnviar
            {
                Body = body,
                DestinoNome = "Fernando José",
                DestinoEmail = "fernandogjose@gmail.com",
                Assunto = "Solicitação demais informações para o app",
                RemetenteNome = entreEmContato.Nome,
                RemetenteEmail = entreEmContato.Email
            };

            return EnviarEmailService.Enviar(emailEntity);
        }
    }
}
